#!/bin/bash
sudo chown -R $(whoami) $HOME

if [ "$ENV" = "WALLET" ]; then
	echo "Wallet mode"
	snowgemd
elif [ "$ENV" = "REINDEX" ]; then
	echo "Reindex mode"
	snowgemd -reindex
elif [ "$ENV" = "TEST" ]; then
	echo "TEST MODE"
	sleep 10000000
else
	echo "Masternode mode"
	snowgemd
fi
