
build:
	docker-compose build --no-cache

stop:
	docker-compose stop
	# docker-compose exec snowgem snowgem-cli stop

start:
	docker-compose up -d

bash:
	docker-compose exec snowgem bash


get-state:
	docker-compose exec snowgem snowgem-cli getinfo

get-balance:
	docker-compose exec snowgem snowgem-cli getbalance

get-addresses:
	docker-compose exec snowgem snowgem-cli listaddressgroupings

fix_permissions:
	sudo find ./data -type d -exec chmod 777 {} \;
	sudo find ./data -type f -exec chmod 666 {} \;
	echo "Data permissions fixed!"

prepare_data: fix_permissions
	p7zip -k ./data 
	# read -p 'Enter version number' version
	# docker build  

