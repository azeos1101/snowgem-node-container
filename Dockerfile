# Snowgem
FROM ubuntu:18.04

RUN apt-get update && \
	apt-get install -y apt-utils && \
	apt-get upgrade -y && \
	apt-get install -y sudo build-essential pkg-config libc6-dev m4 g++-multilib autoconf libtool ncurses-dev unzip git python python-zmq zlib1g-dev wget bsdmainutils automake curl unzip

ADD ./bin/* /usr/local/bin/

EXPOSE 16113 16112

ENV USERNAME snowuser
COPY ./cmd.sh /home/$USERNAME/cmd.sh
RUN groupadd -r $USERNAME && \
	useradd -r -g $USERNAME $USERNAME && \
	chmod +x /home/$USERNAME/cmd.sh
WORKDIR /home/$USERNAME/
RUN echo "$USERNAME ALL=NOPASSWD: ALL" >> /etc/sudoers
USER $USERNAME
